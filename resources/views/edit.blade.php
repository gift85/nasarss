<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $feed->title }}</title>
    </head>
    <body>
        <form method="post">
            <p>You can edit title and description</p>
            {{ csrf_field() }}
            <label for="title">Title</label>
            {{ $errors->first('title') ?? '' }}<br>
            <input type="text" size="80" id="title" name="title" value="{{ old('title', $feed->title) }}"><br>
            <label for="description">Description</label>
            {{ $errors->first('description') ?? '' }}<br>
            <textarea cols="80" rows="10" id="description" name="description">{{ old('description', $feed->description) }}</textarea><br>
            <button>Save</button>
        </form>
    </body>
</html>
