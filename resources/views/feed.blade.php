<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ $feed->title }}</title>
    </head>
    <body>
        {{ $feed->title }}<br>
        <img src="{{ $feed->enclosure }}"><br>
        {{ $feed->description }}<br>
        <a href="{{ $feed->link }}">Original</a><br>
        <hr>
        <form method="post">
            <p>You can say something about this</p>
            {{ csrf_field() }}
            <label for="user">Your name</label>
            {{ $errors->first('user') ?? '' }}<br>
            <input type="text" id="user" name="user" value="{{ old('user', 'Guest') }}"><br>
            <label for="text">What are you thinking about?</label>
            {{ $errors->first('text') ?? '' }}<br>
            <textarea cols="80" rows="10" id="text" name="text">{{ old('text') }}</textarea><br>
            <button>Comment</button>
        </form>
        @forelse($feed->comments as $comment)
            <hr>
            {{ $comment->user }} wrote ({{ $comment->created_at }}):<br>
            {{ $comment->text }}<br>
        @empty
            <p>No comments yet. You can be first</p>
        @endforelse
    </body>
</html>
