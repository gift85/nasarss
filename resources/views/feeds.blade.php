<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Nasa image of the day</title>
    </head>
    <body>
        <table>
            <tr>
                <td>ID</td>
                <td>Actions</td>
                <td>Title</td>
                <td>Publication date</td>
                <td>Upload date</td>
                <td>Comments</td>
            </tr>
            @foreach($feeds as $feed)
                <tr>
                    <td>{{ $feed->id }}</td>
                    <td><a href="{{ route('feed.edit', $feed->id) }}">Edit</a> or <a href="{{ route('feed.remove', $feed->id) }}">Remove</a></td>
                    <td><a href="{{ route('feed.view', $feed->id) }}">{{ $feed->title }}</a></td>
                    <td><a href="{{ $feed->link }}">{{ $feed->pubDate }}</a></td>
                    <td>{{ $feed->created_at }}</td>
                    <td>{{ $feed->comments_count }}</td>
                </tr>
            @endforeach
        </table>
    </body>
</html>
