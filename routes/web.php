<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/update_feed', 'FeedController@update')
    ->name('feed.update');

Route::get('/', 'FeedController@index')
    ->name('feed.index');

Route::get('/view/{id}', 'FeedController@view')
    ->name('feed.view');

/** add comment route */
Route::post('/view/{id}', 'FeedController@comment')
    ->name('feed.comment.new');

Route::get('/edit/{id}', 'FeedController@edit')
    ->name('feed.edit');

Route::post('/edit/{id}', 'FeedController@editPost')
    ->name('feed.edit.save');

Route::get('/remove/{id}', 'FeedController@delete')
    ->name('feed.remove');
