<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Just Feed model
 *
 * Class Feed
 * @package App\Models
 */
class Feed extends Model
{
    /** @var array list of names of fields wich would be protected of mass asignment */
    protected $guarded = [];

    public function comments()
    {
        return $this->hasMany('App\Models\Comment');
    }
}
