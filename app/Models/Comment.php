<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


/**
 * Comment model
 *
 * Class Comment
 * @package App\Models
 */
class Comment extends Model
{
    /** @var array list of names of fields wich would be protected of mass asignment */
    protected $guarded = [];

    public function feed()
    {
        return $this->belongsTo('App\Models\Feed');
    }
}
