<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Feed;
use Carbon\Carbon;
use Illuminate\Http\Request;


/**
 * Manipuliting with our "nasa rss feed"
 *
 * Class FeedController
 * @package App\Http\Controllers
 */
class FeedController extends Controller
{
    /** @var string link to xml document */
    protected $feedUrl = 'https://www.nasa.gov/rss/dyn/lg_image_of_the_day.rss';

    /**
     * parse url and save data to Feeds table
     * pretty ugly, but works
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $feed = simplexml_load_file($this->feedUrl);

        $items = [];
        $dates = [];

        foreach($feed->channel->item as $item) {
            $item->enclosure = $item->enclosure->attributes()->url;
            $item->pubDate = Carbon::createFromFormat('D, d M Y H:i T', $item->pubDate)->toDateTimeString();
            unset($item->guid);
            unset($item->source);
            $items[] = (array)$item;
            $dates[] = (string)$item->pubDate;
        }

        array_multisort($dates, SORT_ASC, $items);

        foreach ($items as $item){
            Feed::firstOrCreate((array)$item);
        }
        return redirect()->route('feed.index');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $feeds = Feed::orderBy('pubDate', 'desc')->withCount('comments')->get();
        return view('feeds', compact('feeds'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($id)
    {
        $feed = Feed::with('comments')->find($id);
        return view('feed', compact('feed'));
    }

    /**
     * validate and save comment
     *
     * @param Request $request
     * @param Comment $comment
     * @param         $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function comment(Request $request, Comment $comment, $id)
    {
        $this->validate($request, [
            'user' => 'required|max:191',
            'text' => 'required',
        ]);

        $comment->feed_id = $id;
        $comment->fill($request->all())->save();

        return redirect()->route('feed.view', $id);
    }

    /**
     * form to edit single feed parts
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $feed = Feed::find($id);
        return view('edit', compact('feed'));
    }

    /**
     * validate and save edited feed
     *
     * @param Request $request
     * @param         $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function editPost(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required|max:191',
            'description' => 'required',
        ]);

        $feed = Feed::find($id);
        $feed->fill($request->all())->save();

        return redirect()->route('feed.view', $id);
    }

    /**
     * remove single feed from db
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id)
    {
        Feed::destroy($id);

        return redirect()->route('feed.index');
    }
}
